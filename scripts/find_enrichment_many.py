#!/usr/bin/env python
# -*- cofing: utf-8 -*-

"""
python %prog study.file population.file gene-association.file

This program returns P-values for functional enrichment in a cluster of study genes using Fisher's exact test, and corrected for multiple testing (including Bonferroni, Holm, Sidak, and false discovery rate)
"""

import sys
import os.path as op
sys.path.insert(0, op.join(op.dirname(__file__), ".."))
from goatools import GOEnrichmentStudy
from goatools.obo_parser import GODag

import StringIO


def read_geneset(study_fin, pop_fn, compare=False):
    pop = set(_.strip() for _ in pop_fin if _.strip())
    study = frozenset(_.strip() for _ in study_fin if _.strip())
    # some times the pop is a second group to compare, rather than the population
    # in that case, we need to make sure the overlapping terms are removed first
    if compare:
        common = pop & study
        pop |= study
        pop -= common
        study -= common
        print >>sys.stderr, "removed %d overlapping items" % (len(common), )

    return study, pop


def read_associations(assoc_fn):
    assoc = {}
    for row in open(assoc_fn):
        atoms = row.split()
        if len(atoms) == 2:
            a, b = atoms
        elif len(atoms) > 2 and row.count('\t')==1:
            a, b = row.split("\t")
        else:
            continue
        b = set(b.replace(";"," ").split())
        assoc[a] = b

    return assoc


def check_bad_args(args):
    """check args. otherwise if one of the 3 args is bad
    it's hard to tell which one"""
    import os
    if not len(args) == 3: return "please send in 3 file names"
    for arg in args[:-1]:
        if not os.path.exists(arg):
            return "*%s* does not exist" % arg

    return False


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--alpha', default=None, type=float)
    parser.add_argument('--compare', default=False, action='store_true')
    parser.add_argument('--ratio', default=None, type=float)
    parser.add_argument('--fdr', default=False, action='store_true')
    parser.add_argument('--indent', default=False, action='store_true')
    parser.add_argument('--go-obo', default='gene_ontology.1_2.obo')
    parser.add_argument('studies')
    parser.add_argument('population')
    parser.add_argument('association')
    opts = parser.parse_args()

    alpha = float(opts.alpha) if opts.alpha else 0.05

    min_ratio = opts.ratio
    if not min_ratio is None:
        assert 1 <= min_ratio <= 2

    assoc = read_associations(opts.association)

    with open(opts.population) as fin:
        population_text = fin.read()

    methods=["bonferroni", "sidak", "holm"]
    if opts.fdr:
        methods.append("fdr")

    obo_dag = GODag(obo_file=opts.go_obo)

    with open(opts.studies) as fin:
        for index, line in enumerate(fin):
            print '>Cluster {:05}'.format(index)

            study_fin = StringIO.StringIO()
            cols = line.split()
            for member in cols[1:]: print >>study_fin, int(member)
            study_fin.seek(0)

            pop_fin = StringIO.StringIO(population_text)

            study, pop = read_geneset(study_fin, pop_fin, compare=opts.compare)

            g = GOEnrichmentStudy(pop, assoc, obo_dag, alpha=alpha, study=study, methods=methods)
            g.print_summary(min_ratio=min_ratio, indent=opts.indent)
